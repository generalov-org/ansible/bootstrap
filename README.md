# bootstrap hetzner hardware



## Prepare

Ensure file `./inventory/sample/hosts.ini` filled like

```ini
[all]
srv-01 ansible_host=1.1.1.1
srv-02 ansible_host=1.1.1.2

[bootstrap]
srv-01
srv-02
```

Edit `./inventory/sample/group_vars/bootstrap.yml` with valid hetzner credentials, as minimal. (provided u/p is generated values).

```yaml
hetzner:
  reboot: true
  reboot_type: hw
  reboot_timeout: 180
  username: "#ws+01234567"
  password: ""
```

And add ansible id_rsa.pub to `root_ssh_keys`.

### Understanding variables

Example provided in `group_vars/bootstrap.yml`.

#### .disks

- `type`: dict
- `keys`: [partition, mdadm, mount]

#### .disks.partition

- `type`: dict
- `keys`:
	- `/path/to/blk/device`
		- human-readable device name (will be used for partition name)
			- see `ansible-doc parted` for next values
			- `label`: default('gpt') (Choices: aix, amiga, bsd, dvh, gpt, loop, mac, msdos, pc98, sun)
			- `flags`: default(omit)
			- `part_type`: default('primary') (Choices: extended, logical, primary)
			- `part_start`: default(omit)
			- `part_end`: default(omit)

#### .disks.mdadm

- `type`: dict
- `keys`:
	- `/dev/md0`: path to md device
		  - `level`: default(1) [0,1,4,5,6,10](https://en.wikipedia.org/wiki/Mdadm)
	    - `assume_clean`: true [true, false]
	    - `metadata`: default(0.90) []float
	    - `devices`: []string

#### .disks.mount

- `type`: dict
- `keys`:
	- `mountpoint`: `/`
		- `src`: like `/dev/md2`, UUID for this device will be auto-detected
		- `options`: default('defaults')
		- `filesystem`: default('ext4')
		- `filesystem_create_cmd`: default('yes | mkfs.' + .filesystem|default('ext4'))

#### .root_ssh_keys

- `type`: list
- `items`: `default([- "{{ lookup('file', lookup('env','HOME') + '/.ssh/id_rsa.pub') }}"])`

#### .debootstrap

- `type`: dict
- `keys`:
  - `dist`: bullseye
  - `backports`: default(false) [false, true]
  - `mirror`: default('http://deb.debian.org/debian')
	- `options`: default('--components=main,contrib,non-free --arch=amd64 --no-check-certificate --no-check-gpg')
  - `packages`: []string

#### .GRUB_NO_SECURITY

- `type`: string
- `value`: `default('noibrs noibpb nopti nospectre_v2 nospectre_v1 l1tf=off nospec_store_bypass_disable no_stf_barrier mds=off mitigations=off')`

#### .GRUB_CMDLINE_LINUX_DEFAULT

- `type`: string
- `value`: `default('nomodeset consoleblank=0 swapaccount=1 cgroup_enable=memory net.ifnames=1 biosdevname=0 apparmor=0 selinux=0 {{ GRUB_NO_SECURITY|default('') }}')`


#### networking examples

##### static with detect ip's

```yaml
networking:
  type: static-auto
```

##### dhcp

```yaml
networking:
  type: dhcp
```

###### manual provision files for systemd-networkd

```yaml
networking:
  type: manual
  manual:
    enp8s0.network: |
      [Match]
      Name=enp8s0
      # MACAddress=a8:a1:59:15:2a:87
      
      [Network]
      Address=1.1.223.38/26
      Gateway=1.1.223.1
      DNS=1.1.98.98
      VLAN=enp8s0.4000
    enp8s0.4000.network: |
      [Match]
      Name=enp8s0.4000
      
      [Network]
      Address=192.168.100.23/24
    enp8s0.4000.netdev: |
      [NetDev]
      Name=enp8s0.4000
      Kind=vlan
      
      [VLAN]
      Id=4000
```

##### static vlan

```yaml

networking:
  type: static-vlan
  vlan_base: 192.168
  vlan_subnet: 24
  vlan:
    4001: {}

networking:
  type: static-vlan
  vlan_base: 10.10.1
  vlan_subnet: 24
  vlan:
    4000:
      Address: "{{ ip }}/24"

networking:
  type: static-vlan
  vlan_base: 192.168
  vlan_subnet: 24
  vlan:
    4000: {}
    # Address: 192.168.4.1/24
    # Address: 192.168.4.2/24
    4001:
      Address: 192.168.100.23/24

networking:
  type: static-vlan
  vlan_base: 192.168
  vlan_subnet: 24
  vlan:
    4000: {}
    # Address: 192.168.4.1/24
    # Address: 192.168.4.2/24
    4001: {}
    # Address: 192.168.41.1/24
    # Address: 192.168.41.2/24
```

## bootstrap

**Warning: All data on server will be erased**

Last lines in your `~/.ssh/config` must be.

```
Host *
  User root
  StrictHostKeyChecking=no
```

Run installation `ansible-playbook -i inventory/sample/hosts.ini bootstrap.yml --skip-tags=prompt,debug`. Process can take up to 15 minutes.

## Post-install checks

Verify `lsblk -f` output.

![lsblk -f](doc/img/lsblk.png)

Verify `cat /etc/systemd/journald.conf` output.

![journald.conf](doc/img/journald.png)

Verify `htop -d 2` output.

![htop -d 2](doc/img/htop.png)

#### kubespray over bootstraped nodes

- run `cluster.yaml`
- [one node cluster] `kubectl -n kube-system scale deployment dns-autoscaler --replicas=0`
- [one node cluster] `kubectl -n kube-system scale deployment coredns --replicas=1`
- [k8s-svc] `kubectl apply -f https://raw.githubusercontent.com/egeneralov/local-path-provisioner/master/deploy/local-path-storage.yaml`
	- `kubectl -n local-path-storage patch deployment local-path-provisioner -p '{"spec":{"template":{"spec":{"$setElementOrder/containers":[{"name":"local-path-provisioner"}],"containers":[{"image":"egeneralov/local-path-provisioner:v0.0.19-patched","name":"local-path-provisioner"}]}}}}'`
- [k8s-svc] `helm install --namespace kube-system --name metrics-server stable/metrics-server`
  - `kubectl -n kube-system patch deployment metrics-server -p '{"spec":{"template":{"spec":{"$setElementOrder/containers":[{"name":"metrics-server"}],"containers":[{"command":["/metrics-server","--logtostderr","--cert-dir=/tmp","--secure-port=8443","--kubelet-preferred-address-types=InternalIP","--kubelet-insecure-tls","--metric-resolution=60s"],"name":"metrics-server"}]}}}}'`
